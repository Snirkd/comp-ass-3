(load "semantic-analyzer.scm")

(display "==================================================================================")
(newline)
(display "==================================================================================")
(newline)
(display "======================== Test Suite for Comp Assignment 3 ========================")
(newline)
(display "============================= Made by: Snir Kadosh ===============================")
(newline)
(display "=============================== As tradition goes: ===============================")
(newline)
(display "=========================== May the Force be with you. ===========================")
(newline)
(display "==================================================================================")
(newline)
(display "==================================================================================")
(newline)
(newline)







;; Format : car: input , cdr: expected output.
(define testItems-remove-applic
	(list
		(cons
			'(applic
				(lambda-simple
					()
					(applic
						(box-get (var fact))
						((const 5))))
				())

			'(applic
				(box-get (var fact))
				((const 5)))
		)

		(cons
			'(applic
				(lambda-simple
					(a)
					(seq (
						(set (var a) (const 1)) 
						(applic
							(lambda-simple
								()
								(var a))
							()))))
				((const #f)))

			'(applic
				(lambda-simple (a) (seq ((set (var a) (const 1)) (var a))))
				((const #f)))
		)

		(cons
			'(applic
				(lambda-simple
					(a) 
					(seq (
						(set (var a) (lambda-simple (x) (var x))) 
						(applic 
							(lambda-simple
								()
								(var a))
							())))) 
				((const #f)))

			'(applic
				(lambda-simple
					(a)
					(seq (
						(set (var a) (lambda-simple (x) (var x)))
						(var a))))
				((const #f)))
		)

		(cons
			'(lambda-simple
				(x)
				(applic
					(lambda-simple
						(y)
						(const 1)) 
				((const 1))))

			'(lambda-simple
				(x)
				(applic
					(lambda-simple
						(y)
						(const 1))
				((const 1))))
		)

		(cons
			'(applic
				(lambda-simple
					(fact)
					(seq (
						(set (var fact) (box (var fact)))
						(box-set
							(var fact)
							(lambda-simple
								(n)
								(if3
									(applic (var zero?) ((var n)))
									(const 1)
									(applic
										(var *)
										((var n)
										(applic
											(box-get (var fact))
											((applic
												(var -)
												((var n)
												(const 1))))))))))
						(applic
							(lambda-simple
								()
								(applic
									(box-get (var fact))
									((const 5)))) 
							()))))
				((const #f)))

			'(applic
				(lambda-simple
					(fact)
					(seq (
						(set (var fact) (box (var fact)))
						(box-set
							(var fact)
							(lambda-simple
								(n)
								(if3 (applic (var zero?) ((var n)))
									(const 1)
									(applic
										(var *)
										((var n)
										(applic
											(box-get (var fact))
											((applic (var -) ((var n) (const 1))))))))))
						(applic (box-get (var fact)) ((const 5))))))
				((const #f)))
		)

		(cons
			'(applic
				(lambda-simple
					(a)
					(seq (
						(set (var a) (lambda-simple (x) (var x)))
						(applic
							(lambda-simple
								()
								(applic
									(lambda-simple
										()
										(applic
											(box-get (var fact))
											((const 5))))
									()))
							()))))
				((const #f)))

			'(applic
				(lambda-simple
					(a)
					(seq (
						(set (var a) (lambda-simple (x) (var x)))
						(applic (box-get (var fact)) ((const 5))))))
				((const #f)))
		)

		(cons
			'(applic
				(lambda-simple
					(a)
					(seq (
						(set (var a) (lambda-simple (x) (var x)))
						(applic
							(lambda-simple
								()
								(applic
									(var cons)
									((var x)
									(applic
										(lambda-simple
											()
											(applic
												(box-get (var fact))
												((const 5))))
										()))))
							()))))
				((const #f)))

			'(applic
				(lambda-simple
					(a)
					(seq (
						(set (var a) (lambda-simple (x) (var x)))
						(applic
							(var cons)
							((var x)
							(applic
								(box-get (var fact))
								((const 5))))))))
				((const #f)))
		)

	)
)











;; Format : car: input , cdr: expected output.
(define testItems-box-set
	(list
		(cons 
			'(applic
				(lambda-simple (a)
					(applic (var list) (
						(lambda-simple () (var a))
						(lambda-simple () 
							(set (var a) (applic (var +) ((var a) (const 1)))))
						(lambda-simple (b) (set (var a) (var b))))))
				((const 0)))

			'(applic
				(lambda-simple
					(a)
					(seq ((set (var a) (box (var a)))
						(applic
							(var list)
							((lambda-simple () (box-get (var a)))
								(lambda-simple
									()
									(box-set
										(var a)
										(applic (var +) ((box-get (var a)) (const 1)))))
								(lambda-simple (b) (box-set (var a) (var b))))))))
				((const 0)))
			)

		(cons
			'(applic
				(lambda-simple (a)
					(applic (var list) (
						(lambda-simple () (var a))
						(lambda-simple () 
							(set (var a) (applic (var +) ((var a) (const 1)))))
						(lambda-simple (a b) (set (var a) (var b))))))
				((const 0)))

			'(applic
				(lambda-simple
					(a)
					(seq ((set (var a) (box (var a)))
							(applic
								(var list)
								((lambda-simple () (box-get (var a)))
									(lambda-simple
										()
										(box-set
											(var a)
											(applic (var +) ((box-get (var a)) (const 1)))))
									(lambda-simple (a b) (set (var a) (var b))))))))
				((const 0)))
			)

		(cons
			'(applic
				(lambda-simple (a)
					(applic (var list) (
						(lambda-simple () (var a))
						(lambda-simple () 
							(set (var a) (applic (var +) ((var a) (const 1)))))
						(lambda-simple (b) (set (var b) (var a))))))
				((const 0)))

			'(applic
				(lambda-simple
					(a)
					(seq ((set (var a) (box (var a)))
							(applic
								(var list)
								((lambda-simple () (box-get (var a)))
									(lambda-simple
										()
										(box-set
											(var a)
											(applic (var +) ((box-get (var a)) (const 1)))))
									(lambda-simple (b) (set (var b) (box-get (var a)))))))))
				((const 0)))
			)

		(cons
			'(applic
				(lambda-simple
					(a)
					(seq (
						(applic
							(var list)
							((lambda-simple () (var a))
								(lambda-simple
									()
									(set (var a) (applic (var +) ((var a) (const 1)))))
								(lambda-simple (b) (set (var a) (var b)))))
						(var x))))
				((const 0)))

			'(applic
				(lambda-simple
					(a)
					(seq (
						(set (var a) (box (var a)))
						(applic
							(var list)
							((lambda-simple () (box-get (var a)))
								(lambda-simple
									()
									(box-set
										(var a)
										(applic (var +) ((box-get (var a)) (const 1)))))
								(lambda-simple (b) (box-set (var a) (var b)))))
						(var x))))
				((const 0)))
			)

		(cons
			'(applic
				(lambda-opt (a) d
					(applic (var list) (
						(lambda-simple () (var a))
						(lambda-simple () 
							(set (var a) (applic (var +) ((var a) (var d) (const 1)))))
						(lambda-simple (b) (set (var a) (var b)))
						(lambda-simple () (set (var d) (const 5))))))
				((const 0)))

			'(applic
				(lambda-opt
					(a)
					d
					(seq (
						(set (var a) (box (var a)))
						(set (var d) (box (var d)))
						(applic
							(var list)
							((lambda-simple () (box-get (var a)))
								(lambda-simple
									()
									(box-set
										(var a)
										(applic
											(var +)
											((box-get (var a)) (box-get (var d)) (const 1)))))
								(lambda-simple (b) (box-set (var a) (var b)))
								(lambda-simple () (box-set (var d) (const 5))))))))
				((const 0)))
			)

		(cons
			'(applic
				(lambda-simple (a)
					(applic (var list) (
						(var a)
						(lambda-simple () 
							(set (var a) (applic (var +) ((const 2) (const 1)))))
						(lambda-simple (b) (set (var a) (var b))))))
				((const 0)))

			'(applic
				(lambda-simple
					(a)
					(seq ((set (var a) (box (var a)))
							(applic
								(var list)
								((box-get (var a))
									(lambda-simple
										()
										(box-set (var a) (applic (var +) ((const 2) (const 1)))))
									(lambda-simple (b) (box-set (var a) (var b))))))))
				((const 0)))
			)

		(cons
			'(applic
				(lambda-simple (a)
					(applic (var list) (
						(lambda-simple () 
							(set (var a) (applic (var +) ((const 2) (const 1)))))
						(lambda-simple (b) (set (var a) (var b))))))
				((const 0)))

			'(applic
				(lambda-simple
					(a)
					(applic
						(var list)
						((lambda-simple
								()
								(set (var a) (applic (var +) ((const 2) (const 1)))))
							(lambda-simple (b) (set (var a) (var b))))))
				((const 0)))
			)

		(cons
			'(applic
				(lambda-simple (a)
					(applic (var list) (
						(lambda-simple () (var a))
						(lambda-simple (a) 
							(set (var a) (applic (var +) ((var a) (const 1)))))
						(lambda-simple (a b) (set (var a) (var b))))))
				((const 0)))	
		
			'(applic
				(lambda-simple
					(a)
					(applic
						(var list)
						((lambda-simple () (var a))
							(lambda-simple
								(a)
								(set (var a) (applic (var +) ((var a) (const 1)))))
							(lambda-simple (a b) (set (var a) (var b))))))
				((const 0)))
		)
		
	)
)















(define testItems-pe->lex-pe
	(list
		(cons
			'(applic
				(var x)
				((lambda-simple
					(x)
					(applic
						(var x)
						((lambda-simple
							()
							(applic
								(var x)
								((lambda-simple () (applic (var x) ((var x))))))))))))

			'(applic
				(fvar x)
				((lambda-simple
					(x)
					(applic
						(pvar x 0)
						((lambda-simple
							()
							(applic
								(bvar x 0 0)
								((lambda-simple
									()
									(applic (bvar x 1 0) ((bvar x 1 0))))))))))))
		)

		(cons
			'(lambda-simple
				(a b)
				(lambda-simple
					(c)
					(applic (var +) ((var a) (var b) (var c)))))

			'(lambda-simple
				(a b)
				(lambda-simple
					(c)
					(applic (fvar +) ((bvar a 0 0) (bvar b 0 1) (pvar c 0)))))
		)

		(cons
			'(lambda-opt
				(a b)
				d
				(lambda-simple
					(c)
					(applic
						(var cons)
						((applic (var +) ((var a) (var b) (var c))) (var d)))))

			'(lambda-opt
				(a b)
				d
				(lambda-simple
					(c)
					(applic
						(fvar cons)
						((applic (fvar +) ((bvar a 0 0) (bvar b 0 1) (pvar c 0)))
							(bvar d 0 2)))))
		)

		(cons
			'(define (var fact)
				(lambda-simple
					(n)
					(if3 (applic (var zero?) ((var n)))
						(const 1)
						(applic
							(var *)
							((var n)
							(applic
								(var fact)
								((applic (var -) ((var n) (const 1))))))))))

			'(define (fvar fact)
				(lambda-simple
					(n)
					(if3 (applic (fvar zero?) ((pvar n 0)))
						(const 1)
						(applic
							(fvar *)
							((pvar n 0)
							(applic
								(fvar fact)
								((applic (fvar -) ((pvar n 0) (const 1))))))))))
		)

		(cons
			'(applic
				(lambda-simple
					(a)
					(seq ((set (var a) (box (var a)))
						(applic
							(var list)
							((lambda-simple () (box-get (var a)))
								(lambda-simple
									()
									(box-set
										(var a)
										(applic (var +) ((box-get (var a)) (const 1)))))
								(lambda-simple (b) (box-set (var a) (var b))))))))
				((const 0)))

			'(applic
				(lambda-simple
					(a)
					(seq ((set (pvar a 0) (box (pvar a 0)))
							(applic
								(fvar list)
								((lambda-simple () (box-get (bvar a 0 0)))
									(lambda-simple
										()
										(box-set
											(bvar a 0 0)
											(applic (fvar +) ((box-get (bvar a 0 0)) (const 1)))))
									(lambda-simple (b) (box-set (bvar a 0 0) (pvar b 0))))))))
				((const 0)))
		)

		(cons
			'(applic
				(lambda-simple
					(a)
					(seq (
						(applic
							(var list)
							((lambda-simple () (var a))
								(lambda-simple
									()
									(set (var a) (applic (var +) ((var a) (const 1)))))
								(lambda-simple (b) (set (var a) (var b)))))
						(var x))))
				((const 0)))

			'(applic
				(lambda-simple
					(a)
					(seq ((applic
							(fvar list)
							((lambda-simple () (bvar a 0 0))
								(lambda-simple
									()
									(set (bvar a 0 0)
										(applic (fvar +) ((bvar a 0 0) (const 1)))))
								(lambda-simple (b) (set (bvar a 0 0) (pvar b 0)))))
						(fvar x))))
				((const 0)))
		)

		(cons
			'(applic
				(lambda-opt
					(a)
					d
					(seq (
						(set (var a) (box (var a)))
						(set (var d) (box (var d)))
						(applic
							(var list)
							((lambda-simple () (box-get (var a)))
								(lambda-simple
									()
									(box-set
										(var a)
										(applic
											(var +)
											((box-get (var a)) (box-get (var d)) (const 1)))))
								(lambda-simple (b) (box-set (var a) (var b)))
								(lambda-simple () (box-set (var d) (const 5))))))))
				((const 0)))

			'(applic
				(lambda-opt
					(a)
					d
					(seq ((set (pvar a 0) (box (pvar a 0)))
							(set (pvar d 1) (box (pvar d 1)))
							(applic
								(fvar list)
								((lambda-simple () (box-get (bvar a 0 0)))
									(lambda-simple
										()
										(box-set
											(bvar a 0 0)
											(applic
												(fvar +)
												((box-get (bvar a 0 0))
													(box-get (bvar d 0 1))
													(const 1)))))
									(lambda-simple (b) (box-set (bvar a 0 0) (pvar b 0)))
									(lambda-simple () (box-set (bvar d 0 1) (const 5))))))))
				((const 0)))
		)

	)
)
















(define testItems-annotate-tc
	(list
		(cons
			'(lambda-simple (x) (applic (var x) ((var x))))

			'(lambda-simple (x) (tc-applic (var x) ((var x))))
		)

		(cons
			'(define (var fact)
				(lambda-simple
					(n)
					(if3 (applic (var zero?) ((var n)))
						(const 1)
						(applic
							(var *)
							((var n)
							 (applic
								(var fact)
								((applic (var -) ((var n) (const 1))))))))))

			'(define (var fact)
				(lambda-simple
					(n)
					(if3 (applic (var zero?) ((var n)))
						(const 1)
						(tc-applic
							(var *)
							((var n)
							 (applic
								(var fact)
								((applic (var -) ((var n) (const 1))))))))))
		)

		(cons
			'(applic
				(var x)
				((lambda-simple
					(x)
					(applic
						(var x)
						((lambda-simple
							()
							(applic
								(var x)
								((lambda-simple () (applic (var x) ((var x))))))))))))

			'(applic
				(var x)
				((lambda-simple
					(x)
					(tc-applic
						(var x)
						((lambda-simple
							()
							(tc-applic
								(var x)
								((lambda-simple () (tc-applic (var x) ((var x))))))))))))
		)

		(cons
			'(lambda-simple
				(f)
				(applic
					(lambda-simple
						(x)
						(applic
							(var f)
							((lambda-opt
								()
								s
								(applic
									(var apply)
									((applic (var x) ((var x))) (var s)))))))
					((lambda-simple
						(x)
						(applic
							(var f)
							((lambda-opt
								()
								s
								(applic
									(var apply)
									((applic (var x) ((var x))) (var s))))))))))

			'(lambda-simple
				(f)
				(tc-applic
					(lambda-simple
						(x)
						(tc-applic
							(var f)
							((lambda-opt
								()
								s
								(tc-applic
									(var apply)
									((applic (var x) ((var x))) (var s)))))))
					((lambda-simple
						(x)
						(tc-applic
							(var f)
							((lambda-opt
								()
								s
								(tc-applic
									(var apply)
									((applic (var x) ((var x))) (var s))))))))))
		)

		(cons
			'(lambda-simple
				(x)
				(if3 (applic (var >) ((var x) (const 5))) (var x) (var x)))

			'(lambda-simple
				(x)
				(if3 (applic (var >) ((var x) (const 5))) (var x) (var x)))
		)


		;;;;;;;;;;;;;;;;;;;;; Chained tests below ;;;;;;;;;;;;;;;;;;;;
		(cons
			(pe->lex-pe (box-set (remove-applic-lambda-nil '(applic
																(lambda-simple
																	()
																	(applic
																		(box-get (var fact))
																		((const 5))))
																()))))

			'(applic (box-get (fvar fact)) ((const 5)))
		)

		(cons
			(pe->lex-pe (box-set (remove-applic-lambda-nil '(applic
																(lambda-simple
																	(a)
																	(seq (
																		(set (var a) (const 1)) 
																		(applic
																			(lambda-simple
																				()
																				(var a))
																			()))))
																((const #f))))))

			'(applic
				(lambda-simple
					(a)
					(seq ((set (pvar a 0) (const 1)) (pvar a 0))))
				((const #f)))
		)

		(cons
			(pe->lex-pe (box-set (remove-applic-lambda-nil '(applic
																(lambda-simple
																	(a) 
																	(seq (
																		(set (var a) (lambda-simple (x) (var x))) 
																		(applic 
																			(lambda-simple
																				()
																				(var a))
																			())))) 
																((const #f))))))

			'(applic
				(lambda-simple
					(a)
					(seq ((set (pvar a 0) (lambda-simple (x) (pvar x 0)))
							(pvar a 0))))
				((const #f)))
		)

		(cons
			(pe->lex-pe (box-set (remove-applic-lambda-nil '(lambda-simple
																(x)
																(applic
																	(lambda-simple
																		(y)
																		(const 1)) 
																((const 1)))))))

			'(lambda-simple
				(x)
				(tc-applic (lambda-simple (y) (const 1)) ((const 1))))
		)

		(cons
			(pe->lex-pe (box-set (remove-applic-lambda-nil '(applic
																(lambda-simple
																	(fact)
																	(seq (
																		(set (var fact) (box (var fact)))
																		(box-set
																			(var fact)
																			(lambda-simple
																				(n)
																				(if3
																					(applic (var zero?) ((var n)))
																					(const 1)
																					(applic
																						(var *)
																						((var n)
																						(applic
																							(box-get (var fact))
																							((applic
																								(var -)
																								((var n)
																								(const 1))))))))))
																		(applic
																			(lambda-simple
																				()
																				(applic
																					(box-get (var fact))
																					((const 5)))) 
																			()))))
																((const #f))))))

			'(applic
				(lambda-simple
					(fact)
					(seq (
						(set (pvar fact 0) (box (pvar fact 0)))
						(box-set (pvar fact 0) (box (box-get (pvar fact 0))))
						(box-set
							(box-get (pvar fact 0))
							(lambda-simple
								(n)
								(if3 (applic (fvar zero?) ((pvar n 0)))
									(const 1)
									(tc-applic
										(fvar *)
										((pvar n 0)
											(applic
												(box-get (box-get (bvar fact 0 0)))
												((applic (fvar -) ((pvar n 0) (const 1))))))))))
						(tc-applic (box-get (box-get (pvar fact 0))) ((const 5))))))
				((const #f)))
		)

		(cons
			(pe->lex-pe (box-set (remove-applic-lambda-nil '(applic
																(lambda-simple
																	(a)
																	(seq (
																		(set (var a) (lambda-simple (x) (var x)))
																		(applic
																			(lambda-simple
																				()
																				(applic
																					(lambda-simple
																						()
																						(applic
																							(box-get (var fact))
																							((const 5))))
																					()))
																			()))))
																((const #f))))))

			'(applic
				(lambda-simple
					(a)
					(seq ((set (pvar a 0) (lambda-simple (x) (pvar x 0)))
						(tc-applic (box-get (fvar fact)) ((const 5))))))
				((const #f)))
		)

		(cons
			(pe->lex-pe (box-set (remove-applic-lambda-nil '(applic
																(lambda-simple
																	(a)
																	(seq (
																		(set (var a) (lambda-simple (x) (var x)))
																		(applic
																			(lambda-simple
																				()
																				(applic
																					(var cons)
																					((var x)
																					(applic
																						(lambda-simple
																							()
																							(applic
																								(box-get (var fact))
																								((const 5))))
																						()))))
																			()))))
																((const #f))))))

			'(applic
				(lambda-simple
					(a)
					(seq ((set (pvar a 0) (lambda-simple (x) (pvar x 0)))
						(tc-applic
							(fvar cons)
							((fvar x) (applic (box-get (fvar fact)) ((const 5))))))))
				((const #f)))
		)


	)
)



(define function-tester
	(lambda
		(test-function test-items)
			(let ((tested-func test-function))
				(letrec 
					((test-func (lambda (lst)
									(if (not (null? lst))
										(begin 
											(let* ((testItem (caar lst))
													(expectedRes (cdar lst))
													(actualRes (tested-func testItem)))
														(display "Test Item:")
														(newline)
														(display testItem)
														(newline)
														;(display "Result:")
														;(newline)
														(if (equal? actualRes expectedRes) ;; comparing our result with the expected.
															(begin 
																(display "\033[1;32mTest Passed. Result:\n\033[0m")
																(display actualRes)
																(newline))
															(begin
																(display "\033[1;31mTest Failed. Result:\n\033[0m ")
																(display actualRes)
																(newline)
																(display "\033[1;31mExpected Result:\n\033[0m ")
																(display expectedRes))))
														(newline)
														(newline)
														(test-func (cdr lst)))))))
									(test-func test-items)))))




(define runAllTests
	(lambda ()
		(display "==================================================================================")
		(newline)
		(display "==========================remove-applic-lambda-nil-tests==========================")
		(newline)
		(display "==================================================================================")
		(newline)
		(newline)
		(function-tester remove-applic-lambda-nil testItems-remove-applic)
		(display "==================================================================================")
		(newline)
		(display "==================================box-set-tests===================================")
		(newline)
		(display "==================================================================================")
		(newline)
		(newline)
		(function-tester box-set testItems-box-set)
		(display "==================================================================================")
		(newline)
		(display "=================================pe->lex-pe-tests=================================")
		(newline)
		(display "==================================================================================")
		(newline)
		(newline)
		(function-tester pe->lex-pe testItems-pe->lex-pe)
		(display "==================================================================================")
		(newline)
		(display "=================================annotate-tc-tests================================")
		(newline)
		(display "==================================================================================")
		(newline)
		(newline)				
		(function-tester annotate-tc testItems-annotate-tc)
	))

;(display "\033[1;32mTest Passed.\033[0m") ;; color scheme for displaying a message in green.
;(display "\033[1;31mFailed!\033[0m ") ;; color scheme to display a message in red.

(runAllTests)

